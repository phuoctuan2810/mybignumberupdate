def tong(str1, str2):
    final = ''
    n1 = len(str1)
    n2 = len(str2)
    maxlen = max(n1, n2)
    sum = 0
    mem = 0
    result = 0
    for (i) in range(maxlen):
        i1 = (n1-i-1)
        i2 = n2-i-1

        if (i1 < 0):
            c1 = 0
        else:
            c1 = ord(str1[i1])-48
        if (i2 < 0):
            c2 = 0
        else:
            c2 = ord(str2[i2])-48
        sum = c1+c2+mem
        print("step", i, (str)(c1) + " + " + (str)
              (c2) + ' + '+(str)(mem) + " = ", sum)
        result = sum % 10
        mem = (int)(sum / 10)
        final = (chr(result+48))+final
    if (mem > 0):
        final = chr(mem+48)+final
    return final


str1 = input()
str2 = input()
print(tong(str1, str2))
